#!/usr/bin/env python

### see https://github.com/enkore/i3pystatus
from i3pystatus import Status
from i3pystatus.updates import pacman, cower
from i3pystatus.weather import weathercom
from i3pystatus.weather import wunderground
import os.path

status = Status(standalone=True)

# Displays clock like this:
# %a %-d %b %X KW%V
# Tue 30 Jul 11:59:46 PM KW31
#                          ^-- calendar week
status.register("clock",
    format="%d.%m. %H:%M",)

## TODO we need something that shows current layout
# status.register("xkblayout")

#status.register("keyboard_locks")

# Shows the average load of the last minute and the last 5 minutes
# (the default value for format is used)
status.register("load")
status.register("mem")

# Shows your CPU temperature, if you have a Intel CPU
if os.path.exists("/sys/class/thermal/thermal_zone0"):
    status.register("temp",
        format="{temp:.0f}°C",)

# The battery monitor has many formatting options, see README for details

# This would look like this, when discharging (or charging)
# ↓14.22W 56.15% [77.81%] 2h:41m
# And like this if full:
# =14.22W 100.0% [91.21%]
#
# This would also display a desktop notification (via D-Bus) if the percentage
# goes below 5 percent while discharging. The block will also color RED.
# If you don't have a desktop notification demon yet, take a look at dunst:
#   http://www.knopwob.org/dunst/
if os.path.exists("/sys/class/power_supply/BAT0"):
  status.register("battery",
    format="{status}/{consumption:.2f}W {percentage:.2f}% [{percentage_design:.2f}%] {remaining:%E%hh:%Mm}",
    alert=True,
    alert_percentage=5,
    status={
        "DIS": "↓",
        "CHR": "↑",
        "FULL": "=",
    },)

# This would look like this:
# Discharging 6h:51m
#status.register("battery",
    #format="{status} {remaining:%E%hh:%Mm}",
    #alert=True,
    #alert_percentage=5,
    #status={
        #"DIS":  "Discharging",
        #"CHR":  "Charging",
        #"FULL": "Bat full",
    #},)

# Displays whether a DHCP client is running
#status.register("runwatch",
    #name="DHCP",
    #path="/var/run/dhclient*.pid",)

# Shows the address and up/down state of eth0. If it is up the address is shown in
# green (the default value of color_up) and the CIDR-address is shown
# (i.e. 10.10.10.42/24).
# If it's down just the interface name (eth0) will be displayed in red
# (defaults of format_down and color_down)
#
# Note: the network module requires PyPI package netifaces
#status.register("network",
    #interface="eno1",
    #format_up="{v4cidr}",)

# Note: requires both netifaces and basiciw (for essid and quality)
#status.register("network",
    #interface="wlan0",
    #format_up="{essid} {quality:03.0f}%",)

# Shows disk usage of /
# Format:
# 42/128G [86G]
#status.register("disk",
    #path="/",
    #format="{used}/{total}G [{avail}G]",)

# Shows pulseaudio default sink volume
#
# Note: requires libpulseaudio from PyPI
status.register("pulseaudio",
    format="♪{volume}",)

status.register("shell",
    command="if [ -f /tmp/.pianobar/nowplaying ]; then cat /tmp/.pianobar/nowplaying ; else echo 'no music' ; fi")

# Shows mpd status
# Format:
# Cloud connected▶Reroute to Remain
#status.register("mpd",
    #format="{title}{status}{album}",
    #status={
        #"pause": "▷",
        #"play": "▶",
        #"stop": "◾",
    #},)

#status.register("weather", location_code="LOXX0094:1:LO")
# status.register(
#     'weather',
#     format='{condition} {current_temp}{temp_unit}{icon}[ Hi: {high_temp}] Lo: {low_temp}',
#     colorize=True,
#     backend=weathercom.Weathercom(
#         location_code='LOXX0094:1:LO',
#         units='metric',
#     ),
# )
status.register(
    'weather',
    format='{condition} {current_temp}{temp_unit}{icon}[ Hi: {high_temp}] Lo: {low_temp}',
    colorize=True,
    backend=wunderground.Wunderground(
        api_key='ff1ee8be992702dc',
        location_code='pws:IZVOLEN7',
        units='metric',
    ),
)
status.register("updates",
                backends = [pacman.Pacman(), cower.Cower()],
                format_no_updates = "current")

status.run()
