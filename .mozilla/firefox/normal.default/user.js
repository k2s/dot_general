# Personal Mozilla User Preferences

user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("xpinstall.signatures.required", false);
user_pref("clipboard.autocopy", true);
user_pref("browser.tabs.closeWindowWithLastTab", false);
user_pref("browser.search.defaultenginename", "DuckDuckGo");

# TreeStyleTab
user_pref("extensions.treestyletab.tabbar.autoHide.mode", 1);
user_pref("extensions.treestyletab.tabbar.autoHide.mode.toggle", 1);
user_pref("extensions.treestyletab.tabbar.invertTab", false);
user_pref("extensions.treestyletab.tabbar.position", "right");
user_pref("extensions.treestyletab.tabbar.style", "mixed");
