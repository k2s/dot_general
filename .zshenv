# Ensure that a non-login, non-interactive shell has a defined environment.
if [[ "$SHLVL" -eq 1 && ! -o LOGIN && -s "${ZDOTDIR:-$HOME}/.zprofile" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprofile"
fi

### terminal
export TERMINAL=termite

### pager 
export PAGER=/usr/bin/less
# if type vimpager > /dev/null; then
# 	#- https://github.com/rkitover/vimpager
# 	export PAGER=vimpager
# 	alias less=$PAGER
# 	alias zless=$PAGER
# fi
export VIEWER="$PAGER"

### editor
export EDITOR=vim
if [ -n "$DISPLAY" ]; then
	if type "sublime" > /dev/null; then
		export EDITOR='sublime -w'
	fi
	# gvim
fi
export VISUAL="$EDITOR"


### browser
## had bad experience with xdg-open loop implementation
if [ -z "$DISPLAY" ]; then
	BROWSER=w3m
else
	BROWSER=google-chrome-stable:chromium:firefox
	for browser in ${(s,:,)BROWSER}; do # https://www.rosettacode.org/wiki/Tokenize_a_string#Zsh
		if type "$browser" > /dev/null; then
			BROWSER=$browser
			break
		fi
	done
fi
export BROWSER

### midnight commander
export MC_XDG_OPEN=~/bin/g/nohup-open
#export MC_SKIN=modarin256

### add my scripts into PATH, scripts from p override scripts from g repository
export PATH="$HOME/bin/l:$HOME/bin/p:$HOME/bin/g:$PATH"

### npm without sudo
if [ -d "$HOME/.npm-packages" ]; then
	PATH="$HOME/.npm-packages/bin:$PATH"
fi

# vim: filetype=sh
