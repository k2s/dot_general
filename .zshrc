fn_exists()
{
	type $1 | grep -q 'shell function'
}

# Source Prezto if not sourced before
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  fn_exists pmodload || source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# TODO ????
#if [ "$TERM" = "xterm-termite" ]; then
#   if [ -f ~/.config/termite/dircolors ]; then
#     eval $(dircolors ~/.config/termite/dircolors)
#   fi
#fi

if [ -f /etc/profile.d/fzf.zsh ]; then
	# fuzzy finder - https://github.com/junegunn/fzf#useful-examples
	source /etc/profile.d/fzf.zsh
fi

# load general aliases and functions
source ~/bin/g/.aliases

if [ -f ~/bin/p/.zpersonal.zsh ]; then
  # personalization
  source ~/bin/p/.zpersonal.zsh
fi
# vim: filetype=sh