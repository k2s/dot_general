#!/bin/env bash

if [ -n "$GIT_DIR" ]; then
	echo "GIT_DIR set, dangerouse to continue"
	exit 1
fi

### sublime text 3 plugin management
if type subl3 > /dev/null ; then
	PLUGIN_DIR="$HOME/.config/sublime-text-3/Installed Packages"
	PLUGIN_FN="$PLUGIN_DIR/Package Control.sublime-package"
	if [ ! -f "$PLUGIN_FN" ]; then
		mkdir -p "$PLUGIN_DIR"
		curl https://packagecontrol.io/Package%20Control.sublime-package > "$PLUGIN_FN"
	fi
fi	

### Telegram Desktop clean up
rm -f $HOME/.local/share/applications/telegramdesktop.desktop

### SpiderOAK
if [[ -d "$HOME/SpiderOak Hive/bin" && ! -e "$HOME/bin/l" ]]; then
	echo "linking SpiderOak bin folder to ~/bin/l"
	ln -s "$HOME/SpiderOak Hive/bin" "$HOME/bin/l"
fi

### SpiderOakONE user setup
if type SpiderOakONE > /dev/null && [ ! -e "$HOME/SpiderOak Hive/" ] ; then
	host=$(hostname)
    reinstall="false"
	echo "enter user name who will be synced to device $host"
	read username
	echo "enter password:"
	read -s password
	cat <<EOF > $HOME/.secret-spideroak
{
    "username": "$username",
    "password": "$password",
    "reinstall": $reinstall,
    "device_name": "$host"
}
EOF
	SpiderOakONE --setup=$HOME/.secret-spideroak
#		rm -f $HOME/.secret-spideroak		
fi

